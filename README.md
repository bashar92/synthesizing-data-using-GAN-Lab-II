# Imbalanced Classification using GANS

In this work, we focused on the augmentation of 4 different datasets with different IR (Imbalance Ratio), 3 of them belong to the GENE data family using different techniques and compare the performance of 3 different classifiers without/with augmentation

<h2>Datasets Use in the experiment</h2>

<table>
    <thead>
        <tr>
            <th>
                Dataset
            </th>
            <th>
                Source
            </th>
            <th>
                Type
            </th>
            <th>
                Imbalanced Ratio
            </th>
        <tr>
        <tbody>
            <tr>
                <td>
                    lung Cancer
                </td>
                <td>
                    NCBI repository
                </td>
                <td>
                    GENE Data
                </td>
                <td>
                    1.13
                </td>
            </tr>
            <tr>
                <td>
                    colon Cancer
                </td>
                <td>
                    UCI repository
                </td>
                <td>
                    GENE Data
                </td>
                <td>
                    1.81
                </td>
            </tr>
            <tr>
                <td>
                    breast Cancer
                </td>
                <td>
                    UCI repository
                </td>
                <td>
                    GENE Data
                </td>
                <td>
                    1.29
                </td>
            </tr>
            <tr>
                <td>
                    bank marketing
                </td>
                <td>
                    UCI repository
                </td>
                <td>
                    Business
                </td>
                <td>
                    7.54
                </td>
            </tr>
        </tbody>
    </thead>
</table>
<h3>Techniques used are</h3>
<ul>
    <li>SMOTE (Synthetic Minority Over-sampling technique)</li>
    <li>Generative Adversarial Networks</li>
    <li>Wasserstein GAN</li>
</ul>
<h3>We evaluated the performance of 3 different classifiers which are</h3>
<ul>
    <li>Logistic Regression</li>
    <li>Decision Tree</li>
    <li>K Nearest Neighbors </li>
</ul>
<h3>metrics evaluated</h3>
<ul>
    <li>accuracy</li>
    <li>Recall</li>
    <li>Precision</li>
    <li>Area under the Receiver Operating Characteristic</li>
</ul>
